import allure
import time

from allure_commons.types import AttachmentType
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException

exec_path='drivers/chromedriver.exe'

class TestMyStoreSignIn:

    def setup(self):
        self.driver = webdriver.Chrome(executable_path=exec_path)\


    def teardown(self):
        self.driver.quit()

    @allure.story('Переход на страницу "Sign in"')
    @allure.severity('critical')
    def test_signin_entry(self):
        with allure.step('переход на сайт'):
            self.driver.get('http://automationpractice.com/index.php')
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot', attachment_type=AttachmentType.PNG)
        with allure.step('переход на страницу "sign in"'):
            signin = self.driver.find_element_by_css_selector('a.login').click()
        with allure.step('Делаем скриншот'):
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot', attachment_type=AttachmentType.PNG)
        assert self.driver.title == 'Login - My Store'


    @allure.story('Регистрация с пустым полем email"')
    @allure.severity('major')
    def test_empty_email(self):
        with allure.step('заходим на сайт'):
            self.driver.get('http://automationpractice.com/index.php')
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot', attachment_type=AttachmentType.PNG)
        with allure.step('заходим нa страницу "sign in"'):
            signin = self.driver.find_element_by_css_selector('a.login').click()
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot', attachment_type=AttachmentType.PNG)
        with allure.step('Переход к созданию аккаунта с пустым email'):
            button = self.driver.find_element_by_css_selector('#SubmitCreate').click()
        time.sleep(5)
        with allure.step('Делаем скриншот'):
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                              attachment_type=AttachmentType.PNG)
        try:
            phrase = self.driver.find_element_by_xpath('//*[@id="create_account_error"]')
        except NoSuchElementException:
            return self.assertTrue(False)
            return True

    @allure.story('Регистрация Email в верхнем регистре"')
    @allure.severity('major')
    def test_up_mail(self):
        with allure.step('заходим на сайт'):
            self.driver.get('http://automationpractice.com/index.php')
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot', attachment_type=AttachmentType.PNG)
        with allure.step('заходим нa страницу "sign in"'):
            signin = self.driver.find_element_by_css_selector('a.login').click()
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot', attachment_type=AttachmentType.PNG)
        with allure.step('кликаем на поле "email"'):
            email_field = self.driver.find_element_by_css_selector('#email_create')
            email_field.click()
        with allure.step('пишем "LAMINOV@mail.ru"'):
            email_field.send_keys("LAMINOV@mail.ru")
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                          attachment_type=AttachmentType.PNG)
        with allure.step('Переход на страницу создания аккаунта'):
            button = self.driver.find_element_by_css_selector('#SubmitCreate').click()
            time.sleep(5)
        with allure.step('Делаем скриншот'):
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                          attachment_type=AttachmentType.PNG)
        try:
            page = self.driver.find_element_by_xpath('//*[@id="account-creation_form"]/div[1]/h3')
        except NoSuchElementException:
            return self.assertTrue(False)
            return True

    @allure.story('Регистрация Email с цифрами в доменной части')
    @allure.severity('major')
    def test_numbers_mail(self):
        with allure.step('заходим на сайт'):
            self.driver.get('http://automationpractice.com/index.php')
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                              attachment_type=AttachmentType.PNG)
        with allure.step('заходим нa страницу "sign in"'):
            signin = self.driver.find_element_by_css_selector('a.login').click()
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                              attachment_type=AttachmentType.PNG)
        with allure.step('кликаем на поле "email"'):
            email_field = self.driver.find_element_by_css_selector('#email_create')
            email_field.click()
        with allure.step('пишем "laminov@123mail.ru"'):
            email_field.send_keys("laminov@123mail.ru")
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                              attachment_type=AttachmentType.PNG)
        with allure.step('Переход на страницу создания аккаунта'):
            button = self.driver.find_element_by_css_selector('#SubmitCreate').click()
            time.sleep(5)
        with allure.step('Делаем скриншот'):
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                              attachment_type=AttachmentType.PNG)
        try:
            page = self.driver.find_element_by_xpath('//*[@id="account-creation_form"]/div[1]/h3')
        except NoSuchElementException:
            return self.assertTrue(False)
            return True

    @allure.story('Регистрация Email с дефисом в доменной части')
    @allure.severity('major')
    def test_defis_mail(self):
        with allure.step('заходим на сайт'):
            self.driver.get('http://automationpractice.com/index.php')
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                          attachment_type=AttachmentType.PNG)
        with allure.step('заходим нa страницу "sign in"'):
            signin = self.driver.find_element_by_css_selector('a.login').click()
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                          attachment_type=AttachmentType.PNG)
        with allure.step('кликаем на поле "email"'):
            email_field = self.driver.find_element_by_css_selector('#email_create')
            email_field.click()
        with allure.step('пишем "laminov@ma-il.ru"'):
            email_field.send_keys("laminov@ma-il.ru")
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                          attachment_type=AttachmentType.PNG)
        with allure.step('Переход на страницу создания аккаунта'):
            button = self.driver.find_element_by_css_selector('#SubmitCreate').click()
            time.sleep(5)
        with allure.step('Делаем скриншот'):
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                          attachment_type=AttachmentType.PNG)
        try:
            page = self.driver.find_element_by_xpath('//*[@id="account-creation_form"]/div[1]/h3')
        except NoSuchElementException:
            return self.assertTrue(False)
            return True

    @allure.story('Регистрация Email без точек в доменной части')
    @allure.severity('major')
    def test_without_point_mail(self):
        with allure.step('заходим на сайт'):
            self.driver.get('http://automationpractice.com/index.php')
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                          attachment_type=AttachmentType.PNG)
        with allure.step('заходим нa страницу "sign in"'):
            signin = self.driver.find_element_by_css_selector('a.login').click()
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                          attachment_type=AttachmentType.PNG)
        with allure.step('кликаем на поле "email"'):
            email_field = self.driver.find_element_by_css_selector('#email_create')
            email_field.click()
        with allure.step('пишем "laminov@mailru"'):
            email_field.send_keys("laminov@mailru")
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                          attachment_type=AttachmentType.PNG)
        with allure.step('Переход на страницу создания аккаунта'):
            button = self.driver.find_element_by_css_selector('#SubmitCreate').click()
            time.sleep(5)
        with allure.step('Делаем скриншот'):
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                          attachment_type=AttachmentType.PNG)
        try:
            phrase = self.driver.find_element_by_xpath('//*[@id="create_account_error"]')
        except NoSuchElementException:
            return self.assertTrue(False)
            return True

    @allure.story('Регистрация с превышением длины email (>320 символов)')
    @allure.severity('major')
    def test_more_than_320_mail(self):
        with allure.step('заходим на сайт'):
            self.driver.get('http://automationpractice.com/index.php')
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                          attachment_type=AttachmentType.PNG)
        with allure.step('заходим нa страницу "sign in"'):
            signin = self.driver.find_element_by_css_selector('a.login').click()
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                          attachment_type=AttachmentType.PNG)
        with allure.step('кликаем на поле "email"'):
            email_field = self.driver.find_element_by_css_selector('#email_create')
            email_field.click()
        with allure.step('пишем email > 320 символов'):
            email_field.send_keys("laminovirklaminovirklaminovirklaminovirklaminovirklaminovirklaminovirklaminovirklaminovirklaminovirklaminovirklaminovirklaminovirklaminovirklaminovirklaminovirklaminovirklaminovirklaminovirklaminovirklaminovirklaminovirklaminovirklaminovirklaminovirklaminovirklaminovirklaminovirklaminovirklaminovirklaminovirklaminovirk@mail.ru")
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                          attachment_type=AttachmentType.PNG)
        with allure.step('Переход на страницу создания аккаунта'):
            button = self.driver.find_element_by_css_selector('#SubmitCreate').click()
            time.sleep(5)
        with allure.step('Делаем скриншот'):
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                          attachment_type=AttachmentType.PNG)
        try:
            phrase = self.driver.find_element_by_xpath('//*[@id="create_account_error"]')
        except NoSuchElementException:
            return self.assertTrue(False)
            return True

    @allure.story('Регистрация с отсутствием @ в email')
    @allure.severity('major')
    def test_without_sobaka_mail(self):
        with allure.step('заходим на сайт'):
            self.driver.get('http://automationpractice.com/index.php')
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                          attachment_type=AttachmentType.PNG)
        with allure.step('заходим нa страницу "sign in"'):
            signin = self.driver.find_element_by_css_selector('a.login').click()
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                          attachment_type=AttachmentType.PNG)
        with allure.step('кликаем на поле "email"'):
            email_field = self.driver.find_element_by_css_selector('#email_create')
            email_field.click()
        with allure.step('laminovmail.ru'):
            email_field.send_keys("laminovmail.ru")
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                          attachment_type=AttachmentType.PNG)
        with allure.step('Переход на страницу создания аккаунта'):
            button = self.driver.find_element_by_css_selector('#SubmitCreate').click()
            time.sleep(5)
        with allure.step('Делаем скриншот'):
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                          attachment_type=AttachmentType.PNG)
        try:
            phrase = self.driver.find_element_by_xpath('//*[@id="create_account_error"]')
        except NoSuchElementException:
            return self.assertTrue(False)
            return True

    @allure.story('Регистрация Email с пробелами в имени пользователя')
    @allure.severity('major')
    def test_with_space_in_username_mail(self):
        with allure.step('заходим на сайт'):
            self.driver.get('http://automationpractice.com/index.php')
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                          attachment_type=AttachmentType.PNG)
        with allure.step('заходим нa страницу "sign in"'):
            signin = self.driver.find_element_by_css_selector('a.login').click()
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                          attachment_type=AttachmentType.PNG)
        with allure.step('кликаем на поле "email"'):
            email_field = self.driver.find_element_by_css_selector('#email_create')
            email_field.click()
        with allure.step('lami nov@mail.ru'):
            email_field.send_keys("lami nov@mail.ru")
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                          attachment_type=AttachmentType.PNG)
        with allure.step('Переход на страницу создания аккаунта'):
            button = self.driver.find_element_by_css_selector('#SubmitCreate').click()
            time.sleep(5)
        with allure.step('Делаем скриншот'):
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                          attachment_type=AttachmentType.PNG)
        try:
            phrase = self.driver.find_element_by_xpath('//*[@id="create_account_error"]')
        except NoSuchElementException:
            return self.assertTrue(False)
            return True

    @allure.story('Регистрация Email с пробелами в доменной части')
    @allure.severity('major')
    def test_with_space_in_domen_mail(self):
        with allure.step('заходим на сайт'):
            self.driver.get('http://automationpractice.com/index.php')
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                          attachment_type=AttachmentType.PNG)
        with allure.step('заходим нa страницу "sign in"'):
            signin = self.driver.find_element_by_css_selector('a.login').click()
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                          attachment_type=AttachmentType.PNG)
        with allure.step('кликаем на поле "email"'):
            email_field = self.driver.find_element_by_css_selector('#email_create')
            email_field.click()
        with allure.step('laminov@ma il.ru'):
            email_field.send_keys("laminov@ma il.ru")
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                          attachment_type=AttachmentType.PNG)
        with allure.step('Переход на страницу создания аккаунта'):
            button = self.driver.find_element_by_css_selector('#SubmitCreate').click()
            time.sleep(5)
        with allure.step('Делаем скриншот'):
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                          attachment_type=AttachmentType.PNG)
        try:
            phrase = self.driver.find_element_by_xpath('//*[@id="create_account_error"]')
        except NoSuchElementException:
            return self.assertTrue(False)
            return True

    @allure.story('Регистрация Email без имени пользователя')
    @allure.severity('major')
    def test_without_username_mail(self):
        with allure.step('заходим на сайт'):
            self.driver.get('http://automationpractice.com/index.php')
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                          attachment_type=AttachmentType.PNG)
        with allure.step('заходим нa страницу "sign in"'):
            signin = self.driver.find_element_by_css_selector('a.login').click()
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                          attachment_type=AttachmentType.PNG)
        with allure.step('кликаем на поле "email"'):
            email_field = self.driver.find_element_by_css_selector('#email_create')
            email_field.click()
        with allure.step('@mail.ru'):
            email_field.send_keys("@mail.ru")
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                          attachment_type=AttachmentType.PNG)
        with allure.step('Переход на страницу создания аккаунта'):
            button = self.driver.find_element_by_css_selector('#SubmitCreate').click()
            time.sleep(5)
        with allure.step('Делаем скриншот'):
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                          attachment_type=AttachmentType.PNG)
        try:
            phrase = self.driver.find_element_by_xpath('//*[@id="create_account_error"]')
        except NoSuchElementException:
            return self.assertTrue(False)
            return True

    @allure.story('Регистрация Email без доменной части')
    @allure.severity('major')
    def test_without_domen_mail(self):
        with allure.step('заходим на сайт'):
            self.driver.get('http://automationpractice.com/index.php')
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                          attachment_type=AttachmentType.PNG)
        with allure.step('заходим нa страницу "sign in"'):
            signin = self.driver.find_element_by_css_selector('a.login').click()
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                          attachment_type=AttachmentType.PNG)
        with allure.step('кликаем на поле "email"'):
            email_field = self.driver.find_element_by_css_selector('#email_create')
            email_field.click()
        with allure.step('laminov@'):
            email_field.send_keys("laminov@")
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                          attachment_type=AttachmentType.PNG)
        with allure.step('Переход на страницу создания аккаунта'):
            button = self.driver.find_element_by_css_selector('#SubmitCreate').click()
            time.sleep(5)
        with allure.step('Делаем скриншот'):
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                          attachment_type=AttachmentType.PNG)
        try:
            phrase = self.driver.find_element_by_xpath('//*[@id="create_account_error"]')
        except NoSuchElementException:
            return self.assertTrue(False)
            return True

    @allure.story('Регистрация с некорректным доменом первого уровня')
    @allure.severity('major')
    def test_uncorrect_domen_1level_mail(self):
        with allure.step('заходим на сайт'):
            self.driver.get('http://automationpractice.com/index.php')
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                          attachment_type=AttachmentType.PNG)
        with allure.step('заходим нa страницу "sign in"'):
            signin = self.driver.find_element_by_css_selector('a.login').click()
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                          attachment_type=AttachmentType.PNG)
        with allure.step('кликаем на поле "email"'):
            email_field = self.driver.find_element_by_css_selector('#email_create')
            email_field.click()
        with allure.step('laminov@mail.ruefrgt'):
            email_field.send_keys("laminov@mail.ruefrgt")
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                          attachment_type=AttachmentType.PNG)
        with allure.step('Переход на страницу создания аккаунта'):
            button = self.driver.find_element_by_css_selector('#SubmitCreate').click()
            time.sleep(5)
        with allure.step('Делаем скриншот'):
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                          attachment_type=AttachmentType.PNG)
        try:
            phrase = self.driver.find_element_by_xpath('//*[@id="create_account_error"]')
        except NoSuchElementException:
            return self.assertTrue(False)
            return True

    @allure.story('Регистрация Email с кириллическим доменным именем')
    @allure.severity('major')
    def test_kirylic_domen_mail(self):
        with allure.step('заходим на сайт'):
            self.driver.get('http://automationpractice.com/index.php')
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                          attachment_type=AttachmentType.PNG)
        with allure.step('заходим нa страницу "sign in"'):
            signin = self.driver.find_element_by_css_selector('a.login').click()
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                          attachment_type=AttachmentType.PNG)
        with allure.step('кликаем на поле "email"'):
            email_field = self.driver.find_element_by_css_selector('#email_create')
            email_field.click()
        with allure.step('laminov@домен.ру'):
            email_field.send_keys("laminov@домен.ру")
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                          attachment_type=AttachmentType.PNG)
        with allure.step('Переход на страницу создания аккаунта'):
            button = self.driver.find_element_by_css_selector('#SubmitCreate').click()
            time.sleep(5)
        with allure.step('Делаем скриншот'):
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                          attachment_type=AttachmentType.PNG)
        try:
            phrase = self.driver.find_element_by_xpath('//*[@id="create_account_error"]')
        except NoSuchElementException:
            return self.assertTrue(False)
            return True

    @allure.story('Регистрация Email с тегом <DROP TABLE>')
    @allure.severity('major')
    def test_tag_mail(self):
        with allure.step('заходим на сайт'):
            self.driver.get('http://automationpractice.com/index.php')
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                          attachment_type=AttachmentType.PNG)
        with allure.step('заходим нa страницу "sign in"'):
            signin = self.driver.find_element_by_css_selector('a.login').click()
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                          attachment_type=AttachmentType.PNG)
        with allure.step('кликаем на поле "email"'):
            email_field = self.driver.find_element_by_css_selector('#email_create')
            email_field.click()
        with allure.step('<DROP TABLE>'):
            email_field.send_keys("<DROP TABLE>")
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                          attachment_type=AttachmentType.PNG)
        with allure.step('Переход на страницу создания аккаунта'):
            button = self.driver.find_element_by_css_selector('#SubmitCreate').click()
            time.sleep(5)
        with allure.step('Делаем скриншот'):
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                          attachment_type=AttachmentType.PNG)
        try:
            phrase = self.driver.find_element_by_xpath('//*[@id="create_account_error"]')
        except NoSuchElementException:
            return self.assertTrue(False)
            return True

